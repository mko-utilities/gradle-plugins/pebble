/*
 * This file was generated by the Gradle 'init' task.
 *
 * The settings file is used to specify which projects to include in your build.
 * For more detailed information on multi-project builds, please refer to https://docs.gradle.org/8.5/userguide/building_swift_projects.html in the Gradle documentation.
 */

// val rootProjectName: String by settings
rootProject.name = "${providers.gradleProperty("rootProjectName").get()}_rootProject"

include("plugin")
project(":plugin").name = providers.gradleProperty("rootProjectName").get()
