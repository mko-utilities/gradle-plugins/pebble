package mko575

import java.io.File
import org.gradle.api.Project
import org.gradle.api.provider.Provider

object Utils {
    
    fun Provider<String>.backedBy(backupProvider:Provider<String>): Provider<String> {
        return this.zip(backupProvider) { a, b -> a ?: b }
    }
    // val pluginOwnerId = providers.gradleProperty("OwnershipId").backedBy(...)
    
    fun fromSubProjOrRootProj(project: Project, propName: String): String {
        val subProjPropName: String = "plugin${propName}"
        val rootProjPropName: String = "rootProject${propName}"
        return if (project.hasProperty(subProjPropName)) {
            project.property(subProjPropName) as String
        } else {
            project.getProviders().gradleProperty(rootProjPropName).get()
        }
    }
    
    fun getPluginInfo(propName: String): String {
        val piFile = File("plugin/src/main/kotlin/io/gitlab/mko_utilities/gradle_plugins/pebble/PluginInfo.kt")
        val regexPattern = Regex("val $propName = \"(.*)\"")
        val matches = regexPattern.find(piFile.readText())
        return matches?.groups?.get(1)?.value ?: ""
    }
    
}