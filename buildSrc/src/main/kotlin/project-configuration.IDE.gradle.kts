
plugins {
    // Apply the IDEA plugin for IntelliJ IDEA users
    idea
    // id "org.jetbrains.gradle.plugin.idea-ext" // version "0.8.1"
    // Apply the Eclipse plugin for Eclipse users
    eclipse
    // Apply the VisualStudio plugin for VisualStudio users
    `visual-studio`
    // Apply the XCode plugin for XCode users
    xcode
}

/**
 * CONFIGURATION RELATED TO: IDE CONFIGURATION
 */

//idea {
//  project {
//    settings {
//      codestyle {
//        hardWrapAt = 80 // Integer,
//        ifForceBraces = FORCE_BRACES_ALWAYS
//        keepControlStatementOnOneLine = true // boolean
//        java { }
//      }
//    }
//  }
//}
