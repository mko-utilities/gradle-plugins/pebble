/**
 * For documentation see https://docs.gradle.org/current/userguide/custom_plugins.html#sec:precompiled_plugins
 */

plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
}