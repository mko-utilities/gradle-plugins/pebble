# Gradle Plugin for Pebble

This project aims at providing a [Gradle](https://gradle.org/) plugin providing easy access to the templating engine [Pebble](https://pebbletemplates.io/), whose syntax is similar to [Jinja](https://jinja.palletsprojects.com)'s.

Its code is inspired by various [Gradle plugins](https://plugins.gradle.org/), including [dev.anies.gradle.template](https://github.com/arocnies/gradle-template).
