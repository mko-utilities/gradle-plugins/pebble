import java.nio.file.FileSystems
import java.nio.file.Path
import io.gitlab.mko_utilities.gradle_plugins.pebble.tasks.DirectoryInstantiationTask

plugins {
    id("io.gitlab.mko575.pebble")
}

pebble {
    includedTemplateSources = files( layout.projectDirectory.dir("src/pebble/includes") )
    dependencies {
        register("helloWorldTemplates") {
            // from(layout.projectDirectory.dir("src/pebble/templates"))
            from = project.fileTree("src/pebble/templates")
            to = layout.buildDirectory.dir("pebble/instances")
            instantiationOperators {
                register("forMitchell") {
                    parameters = mapOf(
                      "name" to "Mitchell"
                    )
                    instanceNaming = {
                        filePath:Path, parameters:Map<String,String> ->
                          FileSystems.getDefault().getPath(
                              parameters.getOrDefault("name",""),
                              filePath.toString()
                          )
                    }
                }
            }
        }
    }
}

tasks.register<DirectoryInstantiationTask>("helloWorldTemplatesForEddy") {
    includedTemplates = files( layout.projectDirectory.dir("src/pebble/includes") )
    templateFileTree = project.fileTree("src/pebble/templates")
    variableInstantiations = mapOf("name" to "Eddy")
    outputDir = layout.buildDirectory.dir("pebble/instances")
    pathModifier = {
        filePath:Path, parameters:Map<String,String> ->
            FileSystems.getDefault().getPath(
                parameters.getOrDefault("name",""),
                filePath.toString()
            )
        }
}