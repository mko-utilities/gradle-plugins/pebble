package io.gitlab.mko_utilities.gradle_plugins.pebble

object TestConfiguration {
    const val FUNCTIONAL_TESTS_RESOURCES = "src/tests/functionalTests/resources"
}