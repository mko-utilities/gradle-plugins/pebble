package io.gitlab.mko_utilities.gradle_plugins.pebble.extensions

import io.pebbletemplates.pebble.lexer.Syntax
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.model.ObjectFactory
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import javax.inject.Inject

abstract class PebblePluginExtension
    @Inject constructor(
        objects: ObjectFactory
    ) : ExtensionAware {
    
    companion object {
        val LOGGER: Logger = Logging.getLogger(PebblePluginExtension::class.java)
    }

    /**
     * String used to open a Pebble comment.
     */
    @Input
    val openCommentDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("{#") }

    /**
     * String used to close a Pebble comment.
     */
    @Input
    val closeCommentDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("#}") }

    /**
     * String used to start a statement to be evaluated by Pebble.
     */
    @Input
    val openEvaluationDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("{%") }

    /**
     * String used to end a statement to be evaluated by Pebble.
     */
    @Input
    val closeEvaluationDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("%}") }

    /**
     * String used to start an expression to be evaluated and printed by Pebble.
     */
    @Input
    val openPrintDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("{{") }

    /**
     * String used to end an expression to be evaluated and printed by Pebble.
     */
    @Input
    val closePrintDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("}}") }

    /**
     * String used to start an expression to be evaluated and printed by Pebble.
     */
    @Input
    val openInterpolationDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("#{") }

    /**
     * String used to end an expression to be evaluated and printed by Pebble.
     */
    @Input
    val closeInterpolationDelimiter: Property<String> =
        objects.property(String::class.java).also{ it.convention("}") }

    /**
     * Provider to access the Pebble Syntax configured.
     */
    val pebbleSyntax: Provider<Syntax> =
        objects.property(Syntax::class.java).also {
            it.convention(
                openCommentDelimiter.flatMap<Syntax?> { openCommentDelim ->
                    openEvaluationDelimiter.flatMap { openEvalDelim ->
                        openPrintDelimiter.flatMap { openPrintDelim ->
                            openInterpolationDelimiter.flatMap { openIterpolDelim ->
                                closeInterpolationDelimiter.flatMap { closeInterpolDelim ->
                                    closePrintDelimiter.flatMap { closePrintDelim ->
                                        closeEvaluationDelimiter.flatMap { closeEvalDelim ->
                                            closeCommentDelimiter.map { closeCommentDelim ->
                                                val sBuilder = Syntax.Builder()
                                                    .setCommentOpenDelimiter(openCommentDelim)
                                                    .setCommentCloseDelimiter(closeCommentDelim)
                                                    .setExecuteOpenDelimiter(openEvalDelim)
                                                    .setExecuteCloseDelimiter(closeEvalDelim)
                                                    .setPrintOpenDelimiter(openPrintDelim)
                                                    .setPrintCloseDelimiter(closePrintDelim)
                                                sBuilder.interpolationOpenDelimiter = openIterpolDelim
                                                sBuilder.interpolationCloseDelimiter = closeInterpolDelim
                                                sBuilder.build()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            )
            it.disallowChanges()
            it.finalizeValueOnRead()
        }

    // var includedTemplatesDirectory: RegularFileProperty = ObjectFactory.fileProperty()
    abstract val includedTemplateSources: ConfigurableFileCollection
    // abstract val includedTemplateSources: ConfigurableFileCollection

    // @get:Nested
    open val instanceDependencies: NamedDomainObjectContainer<InstanceDependencies> =
        objects.domainObjectContainer(InstanceDependencies::class.java).also {
            extensions.add("dependencies", it)
        }

    /**
     * Initializes the provided project according to plugin extension configuration.
     * @param project the Project to initialize
     */
    fun applyTo(project: Project) {
        project.afterEvaluate {
            LOGGER.info("includedTemplateSources = '${includedTemplateSources.asPath}'")
        }
        instanceDependencies.all { it.applyTo(project, this) }
    }

}
