package io.gitlab.mko_utilities.gradle_plugins.pebble.tasks

import io.gitlab.mko_utilities.gradle_plugins.pebble.extensions.PebblePluginExtension
import io.pebbletemplates.pebble.loader.FileLoader
import org.gradle.api.file.FileCollection
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import java.io.File
import java.io.Reader
import java.nio.file.Files

class CustomTemplateLoader constructor(
    private val includedTemplates: FileCollection
): FileLoader() {

    private val expandedListOfTemplates: MutableSet<File> = HashSet()

    companion object {
        val LOGGER: Logger = Logging.getLogger(CustomTemplateLoader::class.java)
    }

    init {
        includedTemplates.forEach { it ->
            if ( it.isFile ) expandedListOfTemplates.add(it)
            if ( it.isDirectory ) {
                expandedListOfTemplates.addAll(
                    it.walk().filter { it.isFile }.toSet()
                )
            }
        }
        LOGGER.info(
            "Initialized Pebble CustomTemplateLoader with ${includedTemplates.files}" +
                    ", found the following templates: ${
                        expandedListOfTemplates.joinToString(", ") { it.name }
                    }"
        )
    }

    override fun getReader(templateName: String): Reader {
        if ( File(templateName).isFile )
            return File(templateName).reader()

        expandedListOfTemplates.find {
            it.name == templateName
        }?.also { return it.reader() }

        throw RuntimeException(
            "Could not find template '$templateName' in: ${includedTemplates.files}"
        )
    }

}