package io.gitlab.mko_utilities.gradle_plugins.pebble.tasks

import io.pebbletemplates.pebble.PebbleEngine
import io.pebbletemplates.pebble.lexer.Syntax
import io.pebbletemplates.pebble.loader.FileLoader
import io.pebbletemplates.pebble.template.PebbleTemplate
import java.io.File
import java.io.FileWriter
import java.io.Writer

private val defaultSyntax: Syntax =
    Syntax.Builder()
        .setCommentOpenDelimiter("(#")
        .setCommentCloseDelimiter("#)")
        .setExecuteOpenDelimiter("(%")
        .setExecuteCloseDelimiter("%)")
        .setPrintOpenDelimiter("((")
        .setPrintCloseDelimiter("))")
        .build()

/**
 * Instantiate a template using {@link https://pebbletemplates.io/ Pebble}.
 * @param templateFile the template file to instantiate
 * @param context a mapping assigning values to variables used in the template
 * @param instanceFile the file into which writing the instance
 */
fun instantiateFile(
    syntax: Syntax,
    includedTemplateLoader: FileLoader,
    templateFile: File,
    context: Map<String, String>?,
    instanceFile: File
) {
    // println("Calling instantiateFile(..., $templateFile, $context, $instanceFile)")
    val engine: PebbleEngine =
        PebbleEngine.Builder()
            .syntax(syntax)
            .loader(includedTemplateLoader)
            .build()
    val template: PebbleTemplate = engine.getTemplate(templateFile.path)
    instanceFile.delete()
    instanceFile.parentFile.mkdirs()
    val outputWriter: Writer = FileWriter(instanceFile)
    template.evaluate(outputWriter, context ?: mapOf())
    outputWriter.close()
}