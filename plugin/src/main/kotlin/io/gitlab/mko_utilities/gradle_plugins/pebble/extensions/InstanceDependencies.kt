package io.gitlab.mko_utilities.gradle_plugins.pebble.extensions

import io.gitlab.mko_utilities.gradle_plugins.pebble.tasks.DirectoryInstantiationTask
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.ConfigurableFileTree
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.model.ObjectFactory
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.provider.Property
import org.gradle.api.tasks.TaskProvider
import org.gradle.configurationcache.extensions.capitalized
import javax.inject.Inject

abstract class InstanceDependencies @Inject constructor (
    objects: ObjectFactory,

    /**
     * Name of the instantiation dependencies
     */
    private val name: String
) : ExtensionAware {

    /**
     * Field to test renaming in configuration DSL
     *
     * TODO: remove after tweaking is over
     */
    val tmpProp: Property<String> =
        objects.property(String::class.java).also {
            extensions.add("newNameForProp", it)
        }

    /**
     * Directories to explore to find templates to process with Pebble
     */
    // abstract val from: ConfigurableFileTree
    lateinit var from: ConfigurableFileTree

    /**
     * Directory where to put the created instances
     */
    abstract val to: DirectoryProperty

    // @get:Nested
    open val instantiationOperators: NamedDomainObjectContainer<InstantiationOperator> =
        objects.domainObjectContainer(InstantiationOperator::class.java).also {
            extensions.add("byInstantiations", it)
        }
    /**
     * Companion object supporting constants
     */
    companion object {
        val LOGGER: Logger = Logging.getLogger(PebblePluginExtension::class.java)
    }

    /**
     * Accessor for the name of the instantiation dependencies
     */
    fun getName(): String {
        return name
    }

    /**
     * Method to initialize necessary tasks for this instantiation dependencies.
     */
    fun applyTo(project: Project, pebbleConf: PebblePluginExtension) {
        project.afterEvaluate {
            val srcStr = from.asPath
            val dstStr = to.get()
            val instOps = instantiationOperators
            val opDescrBuilder = StringBuilder().append(
                "\n - ${if (instOps.size > 0) instOps.size else "no"} instantiation" +
                        " operator${if (instOps.size > 1) "s" else ""}"
            )
            instOps.all { instOp ->
                opDescrBuilder.append("\n   - ${ instOp.toOneLinerString() }")

                val newTaskName: String = "$name${instOp.getName().capitalized()}"
                val newTask: TaskProvider<DirectoryInstantiationTask> =
                    project.tasks.register(newTaskName, DirectoryInstantiationTask::class.java) { instTask ->
                        instTask.includedTemplates.setFrom(pebbleConf.includedTemplateSources)
                        // instTask.templateFileTree.setDir(from.dir)
                        // instTask.templateFileTree.plus(from)
                        instTask.templateFileTree = from
                        instTask.variableInstantiations.set(instOp.parameters)
                        instTask.outputDir.set(to)
                        instTask.pathModifier.set(instOp.instanceNaming)
                    }
                LOGGER.info("Registered tasks '$newTaskName'")
            }
            LOGGER.info(
                "Initializing Pebble instantiation process '$name' with:" +
                        // "\n - tmpProp '${tmpProp.getOrElse("Undefined")}';" +
                        "\n - template sources '${srcStr}';" +
                        "\n - instances destination '${dstStr}';" +
                        opDescrBuilder
            )
        }
    }

}