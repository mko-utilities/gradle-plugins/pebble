/*
 * This file is part of the Gradle plugin for Pebble.
 * <p>
 * Copyright (c) 2024 by M.K.O.
 * <p>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
package io.gitlab.mko_utilities.gradle_plugins.pebble

import org.gradle.api.Project
import org.gradle.api.Plugin

/**
 * Main class of the Gradle plugin for Pebble.
 */
class PebbleBasePlugin: Plugin<Project> {

    override fun apply(project: Project) {
    }

}
