package io.gitlab.mko_utilities.gradle_plugins.pebble.extensions

import org.gradle.api.Action
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.model.ObjectFactory
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import java.nio.file.Path
import javax.inject.Inject

abstract class InstantiationOperator @Inject constructor (
    objects: ObjectFactory,

    /**
     * Name of the instantiation operator
     */
    private val name: String
) : ExtensionAware {

    /**
     * Overloads the directories declared in the parent dependencies
     * {@link InstanceDependencies#from from parameter} to explore to find templates
     * to process with Pebble.
     * @see InstanceDependencies.from
     */
    abstract val from: ConfigurableFileCollection

    /**
     * Overloads the directory declared in the parent dependencies
     * {@link InstanceDependencies#to to parameter} where to put the created
     * instances.
     */
    abstract val to: DirectoryProperty

    /**
     * Parameters to pass to Pebble for processing.
     */
    abstract val parameters: MapProperty<String,String>

    /**
     * Process to apply to compute the name of the output file from the name of
     * the framework file and parameters.
     */
    abstract val instanceNaming: Property<(Path, Map<String, String>) -> Path>
    // As above works only with build scripts written in Kotlin (not Groovy), should move to Action
    // abstract val instanceNaming: Property<Action<String>>

    /**
     * Companion object supporting constants
     */
    companion object {
        val LOGGER: Logger = Logging.getLogger(PebblePluginExtension::class.java)
    }

    /**
     * Accessor for the name of the instantiation operator
     */
    fun getName(): String {
        return name
    }

    /**
     * Describes this dependency instantiation operator on one line.
     * @return a one-liner String describing the operator.
     */
    fun toOneLinerString(): String {
        val fromDescrStr = if (from.isEmpty) null else "from: ${from.asPath}"
        val toDescrStr = to.map{ "to: $it" }.orNull
        // $it works directly, but keep long version to document access
        val parametersDescrStr = parameters.map{ "params: [${it.map{(k,v) -> "$k=$v"}.joinToString(", ")}]" }.orNull
        val namingDescrStr = instanceNaming.map{ "naming: $it" }.orNull
        val allFieldsAsList = listOf(fromDescrStr, toDescrStr, parametersDescrStr, namingDescrStr)
        return "'$name' {${allFieldsAsList.filterNotNull().joinToString("; ")}}"
    }

}