package io.gitlab.mko_utilities.gradle_plugins.pebble.tasks

import io.gitlab.mko_utilities.gradle_plugins.pebble.extensions.PebblePluginExtension
import io.pebbletemplates.pebble.lexer.Syntax
import org.gradle.api.DefaultTask
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.*
import org.gradle.work.ChangeType
import org.gradle.work.Incremental
import org.gradle.work.InputChanges
import java.io.File

// @CacheableTask
abstract class FileInstantiationTask : DefaultTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The Pebble Syntax to be used to parse process files.
     *
     * This property is set "@Internal" because Syntax is not Serializable. As a consequence,
     * its value is not used in up-to-date checks. If you modify Pebble Syntax, you'll need
     * to run a 'clean' task first (or force the task to be run another way).
     *
     * I could have used a MapProperty instead and use it to construct a Syntax object each time,
     * but I do not want to do that. The reason is mainly performance. It may not have much impact,
     * but Pebble's Syntax should not be changed often either.
     */
    @get:Internal
    abstract val pebbleSyntax: Property<Syntax>

    @get:InputFiles
    @get:PathSensitive(PathSensitivity.ABSOLUTE)
    abstract val includedTemplates: ConfigurableFileCollection

    @get:Incremental
    @get:InputFile
    @get:PathSensitive(PathSensitivity.ABSOLUTE)
    abstract val templateFile: RegularFileProperty

    @get:Input
    abstract val variableInstantiations: MapProperty<String, String>

    @get:OutputFile
    abstract val outputFile: RegularFileProperty

    init {
        val pluginExtension: PebblePluginExtension? =
            project.extensions.findByType(PebblePluginExtension::class.java)
        pluginExtension?.also {
            pebbleSyntax.convention(it.pebbleSyntax)
        }
    }

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    /**
     * Action executed by the task.
     */
    @TaskAction
    fun execute(inputChanges: InputChanges) {
        println(
            if (inputChanges.isIncremental) "Executing incrementally"
            else "Executing non-incrementally"
        )

        val includedTemplateLoader = CustomTemplateLoader(includedTemplates)

        inputChanges.getFileChanges(project.files(templateFile)).forEach { change ->
            // This should be useless
            // if (change.fileType == FileType.DIRECTORY) return@forEach

            if (! templateFile.isPresent) {
                logger.warn("Input 'templateFile' of task $name is not present! Skipping task.")
                return
            }

            if (! outputFile.isPresent) {
                logger.warn("Input 'outputFile' of task $name is not present! Skipping task.")
                return
            }

            println("${change.changeType}: ${change.normalizedPath}")
            val targetFile: File = outputFile.get().asFile
            if (change.changeType == ChangeType.REMOVED) {
                targetFile.delete()
            } else {
                instantiateFile(
                    pebbleSyntax.get(),
                    includedTemplateLoader,
                    templateFile.get().asFile,
                    variableInstantiations.get(),
                    targetFile
                )
            }
        }
    }
}