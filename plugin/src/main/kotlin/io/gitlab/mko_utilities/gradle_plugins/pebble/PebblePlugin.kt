/*
 * This file is part of the Gradle plugin for Pebble.
 * <p>
 * Copyright (c) 2024 by M.K.O.
 * <p>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
package io.gitlab.mko_utilities.gradle_plugins.pebble

import io.gitlab.mko_utilities.gradle_plugins.pebble.PluginInfo.PLUGIN_NAME
import io.gitlab.mko_utilities.gradle_plugins.pebble.PluginInfo.PLUGIN_ID
import io.gitlab.mko_utilities.gradle_plugins.pebble.PluginInfo.PLUGIN_INFO_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.pebble.PluginInfo.PLUGIN_VERSION
import io.gitlab.mko_utilities.gradle_plugins.pebble.PluginInfo.PLUGIN_OWNER_ID
import io.gitlab.mko_utilities.gradle_plugins.pebble.PluginInfo.PLUGIN_EXTENSION_ID
import io.gitlab.mko_utilities.gradle_plugins.pebble.extensions.PebblePluginExtension
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

/**
 * Main class of the Gradle plugin for Pebble.
 */
class PebblePlugin: Plugin<Project> {

    companion object {
        val LOGGER: Logger = Logging.getLogger(PebblePlugin::class.java)
    }

    override fun apply(project: Project) {
        with(project) {
            plugins.apply(PebbleBasePlugin::class.java)

            val pluginExtension =
                extensions.create(PLUGIN_EXTENSION_ID, PebblePluginExtension::class.java)
            // pluginExtension.instanceDependencies.register("testName") { ... } // See https://stackoverflow.com/a/76163344
            pluginExtension.applyTo(project)

            // Register plugin info task
            tasks.register(PLUGIN_INFO_TASK_NAME, DefaultTask::class.java) { task ->
                // task.group = PLUGIN_ID
                // task.description = "..."
                // task.extension = pluginExtension
                task.doLast {
                    println(
                        "This is plugin '$PLUGIN_NAME' ($PLUGIN_ID v$PLUGIN_VERSION)" +
                                " of '$PLUGIN_OWNER_ID'" +
                                " in package '${this@PebblePlugin::class.java.`package`.name}'")
                }
            }
        }
    }
}
