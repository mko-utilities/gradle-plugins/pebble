package io.gitlab.mko_utilities.gradle_plugins.pebble.tasks

import io.gitlab.mko_utilities.gradle_plugins.pebble.extensions.PebblePluginExtension
import io.pebbletemplates.pebble.lexer.Syntax
import org.gradle.api.DefaultTask
import org.gradle.api.file.*
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.*
import org.gradle.work.ChangeType
import org.gradle.work.Incremental
import org.gradle.work.InputChanges
import java.io.File
import java.nio.file.Path
import kotlin.io.path.pathString

// @CacheableTask
abstract class DirectoryInstantiationTask : DefaultTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The Pebble Syntax to be used to parse process files.
     *
     * This property is set "@Internal" because Syntax is not Serializable. As a consequence,
     * its value is not used in up-to-date checks. If you modify Pebble Syntax, you'll need
     * to run a 'clean' task first (or force the task to be run another way).
     *
     * I could have used a MapProperty instead and use it to construct a Syntax object each time,
     * but I do not want to do that. The reason is mainly performance. It may not have much impact,
     * but Pebble's Syntax should not be changed often either.
     */
    @get:Internal
    abstract val pebbleSyntax: Property<Syntax>

    /**
     * Pebble template files that can be included in instantiated templates.
     */
    @get:InputFiles
    @get:PathSensitive(PathSensitivity.ABSOLUTE)
    abstract val includedTemplates: ConfigurableFileCollection

    /**
     * Templates tree to be instantiated.
     */
    @get:Incremental
    @get:InputFiles
    @get:PathSensitive(PathSensitivity.ABSOLUTE)
    // abstract val templateFileTree: ConfigurableFileTree
    lateinit var templateFileTree: ConfigurableFileTree

    /**
     * Variable valuations to be applied to templates.
     */
    @get:Input
    abstract val variableInstantiations: MapProperty<String, String>

    /**
     * Root directory where to output instantiated templates tree.
     */
    @get:OutputDirectory
    abstract val outputDir: DirectoryProperty

    /**
     * Renaming to be applied to template file names to get instance file names.
     */
    @get:Input
    abstract val pathModifier: Property<(Path, Map<String, String>) -> Path>

    init {
        val pluginExtension: PebblePluginExtension? =
            project.extensions.findByType(PebblePluginExtension::class.java)
        pluginExtension?.also {
            pebbleSyntax.convention(it.pebbleSyntax)
        }
        pathModifier.convention{ p, _ -> p}
    }

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    /**
     * Action executed by the task.
     */
    @TaskAction
    fun execute(inputChanges: InputChanges) {
        logger.info(
            if (inputChanges.isIncremental) "Executing incrementally"
            else "Executing task '${name}' non-incrementally."
        )

        if (! outputDir.isPresent) {
            logger.warn("Required input 'outputFile' of task $name has not been provided! Skipping task.")
            return
        }

        val includedTemplateLoader = CustomTemplateLoader(includedTemplates)

        inputChanges.getFileChanges(templateFileTree).forEach { change ->
            println("${change.changeType}: ${change.normalizedPath}")

            if (change.fileType == FileType.DIRECTORY) {
                if (change.changeType == ChangeType.REMOVED) {
                    outputDir.dir(change.normalizedPath).get().asFile.delete()
                }
                return@forEach
            }

            val targetFile: File? =
                change.file.relativeToOrNull(templateFileTree.dir)?.toPath()?.let { origRelPath ->
                    pathModifier.get().invoke(origRelPath, variableInstantiations.get()).let { modifiedRelPath ->
                        logger.debug(
                            "For Pebble task '{}', template file '{}' (in {}) relates to instance '{}' (in {}).",
                            name,
                            origRelPath,
                            templateFileTree,
                            modifiedRelPath,
                            outputDir.get()
                        )
                        outputDir.file(modifiedRelPath.pathString).get().asFile
                    }
                }
            if (change.changeType == ChangeType.REMOVED) {
                targetFile?.delete()
            } else {
                logger.quiet("Generating '${targetFile?.relativeTo(outputDir.asFile.get())}'.")
                targetFile?.let {
                    instantiateFile(
                        pebbleSyntax.get(),
                        includedTemplateLoader,
                        change.file,
                        variableInstantiations.getOrElse(mapOf()),
                        it
                    )
                }
            }
        }
    }
}